# TJX Technical Frontend Exercise

This is an exercise to understand a frontend developer's CSS and Vue.JS knowledge.

We would like to see the use of Vue.JS framework and development of tests to protect your code.

Please do NOT use the existing JS and CSS as a guide of the standard. They were written by a backend developer! ;)

The aim of this exercise is not to see who can complete the exercises but to understand how a developer will tackle a problem, and what level of importance is placed on various considerations that are required in high performing production UI.

Clearly this is not a production ready application, but the candidate should demonstrate awareness of high performing and supportable code.

## Getting Started

1. Fork this repository into your own personal repository.
2. Checkout the repository to your local machine
3. Using Terminal or Command Prompt, navigate to the directory where you checked out the repository
4. Build and start application by running: gradlew bootRun.
    - Application has started once you see following output (where X is a digit value): Started App in X.XXX seconds (JVM running for X.XXX)
5. Access website on http://localhost:8080
6. Any changes you make may need you to stop the process started by 4 and running step 4 again.

## Tips
* Strongly recommend you use 1 or more commit per task
* Use appropriate comments in the commit
* Treat each task as if you are developing a story
* We are not interested in how quickly this exercise is complete, but instead the thought process
* The tasks are specifically vague, please make any sensible assumptions you may need to make and comment them in the code
* We are looking more at frontend skills, but if you want to show off any backend skills, you are more than welcome!

## Tasks

### 1. As a user I want the full product list page to match my green colour palette
* Full Product list is shown on product list page: http://localhost:8080/productList or by clicking on "Full Product List" on home page (http://localhost:8080)
* The header row should be filled with colour #4CAF50 with the text white.
* The even numbered row should be filled with colour white with the text black.
* The odd numbered row should be filled with colour #beefc0 with the text black.
* You may modify productList.jsp and add/modify any JS or CSS files.

### 2. As a user I want to search the product list page for products by product name
* Full Product list is shown on product list page: http://localhost:8080/productList or by clicking on "Full Product List" on home page (http://localhost:8080)
* When user enters some text in the green search box on top right and click Search a client side search/filter of product name should be run.
* When search/filter performed only row matching search should display.
    - eg searching on text "Product 1" should display product row for with exact product name "Product 1"
* Search is exact match only, you do not need to worry about partial matching.
* You may use/import any preferred JS libraries.
* This should be pure client side filter and hence no backend calls to be made.
* You may modify productList.jsp and add/modify any JS or CSS files.

### 3. As a user I want to browse products 5 at a time using the show more link
* Paginated Product List is shown on paginated product list page: http://localhost:8080/productListPaginated or by clicking on "Paginated Product List" on home page (http://localhost:8080)                                                                              * 
* Clicking on the green "Show more ..." text in the green section of bottom right should display next 5 products.
* The clicking action should call a server side api using GET to return JSON formatted products based on the inputs as follows:
  * API URL: http://localhost:8080/productListPaginated/json
  * Parameter: 
    * pageNum - Page number (Optional). Default value: 1
    * numOfItems - Number of items to return (Optional). Default value: 5
* You may use/import any preferred JS libraries.
* You may modify productListPaginated.jsp and add/modify any JS or CSS files.

## Complete
Once you are ready to submit your exercise share your repository with read-only access to:
- kishan_bhanderi@tjxeurope.com
- swati_onkar@tjxeurope.com



package com.tjx.ecomm.config;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.populators.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.tjx.ecomm")
public class WebConfig implements WebMvcConfigurer
{
    @Bean
    public InternalResourceViewResolver getInternalResourceViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");

        return resolver;
    }

    // =======================================
    // =          Override Methods           =
    // =======================================

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**")
                .addResourceLocations("/WEB-INF/css/");

        registry.addResourceHandler("/js/**")
                .addResourceLocations("/WEB-INF/js/");
    }

    @Bean(name = "productToProductDataPopulators")
    public List<Populator<Product, ProductData>> productToProductDataPopulators()
    {
        final List<Populator<Product, ProductData>> populators = new ArrayList<>();
        populators.add(new EssentialProductToProductDataPopulator());
        populators.add(new StockInformationProductToProductDataPopulator());

        return populators;
    }

    @Bean(name = "productDataToProductPopulators")
    public List<Populator<ProductData, Product>> productDataToProductPopulators()
    {
        final List<Populator<ProductData, Product>> populators = new ArrayList<>();
        populators.add(new EssentialProductDataToProductPopulator());
        populators.add(new StockInformationProductDataToProductPopulator());

        return populators;
    }
}

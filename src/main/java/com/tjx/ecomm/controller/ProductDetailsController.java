package com.tjx.ecomm.controller;

import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.exceptions.DuplicateProductException;
import com.tjx.ecomm.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ProductDetailsController
{
   @Autowired
   private ProductService productService;

   @RequestMapping("/productDetails/sku/{sku}")
   public String viewProductDetailsPage(@PathVariable final String sku, final Model model)
   {
      final ProductData productData = productService.getProduct(sku);
      model.addAttribute("product", productData);
      return "productDetails";
   }

   @RequestMapping(value = "/productDetails", method = RequestMethod.POST)
   public String saveProduct(@Valid @ModelAttribute("product") ProductData productData, final BindingResult bindingResult, final Model model)
   {
      if (bindingResult.hasErrors())
      {
         model.addAttribute("product", enrich(productData));
         return  "productDetails";
      }

      try
      {
         productService.createOrUpdateMethod(productData);
      }
      catch (final DuplicateProductException duplicateProductException)
      {
         bindingResult.rejectValue("skuCode", "productDetails.skuCode.validation.error");
         return "productDetails";
      }

      return "redirect:/productList";
   }

   @RequestMapping("/addProduct")
   public String addProductPage(final Model model)
   {
      final ProductData productData = new ProductData();
      model.addAttribute("product", productData);
      return "productDetails";
   }

   private ProductData enrich(final ProductData productData)
   {
      if (productData.isPersisted() && productData.getSkuCode() != null && !productData.getSkuCode().startsWith("#"))
      {
         productData.setSkuCode("#" + productData.getSkuCode());
      }
      return productData;
   }
}

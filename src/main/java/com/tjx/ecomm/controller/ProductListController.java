package com.tjx.ecomm.controller;

import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.services.ProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProductListController
{
    final static int DEFAULT_PAGE_NUM = 0;
    final static int DEFAULT_NUM_OF_ITEMS = 5;

    @Autowired
    private ProductService productService;

    @RequestMapping("/productList")
    public String listProducts(final Model model) {

        model.addAttribute("productList", productService.getAllProducts());

        return "productList";

    }

    @RequestMapping("/productListPaginated")
    public String listProductsPaginated(final Model model, @RequestParam (required = false) String pageNum, @RequestParam (required = false) String numOfItems) {

        model.addAttribute("productList", getProductData(pageNum, numOfItems));

        return "productListPaginated";

    }

    @GetMapping("/productListPaginated/json")
    public @ResponseBody
    List<ProductData> listProductsPaginatedJson(final Model model, @RequestParam (required = false) String pageNum, @RequestParam (required = false) String numOfItems) {

        return getProductData(pageNum, numOfItems);

    }

    private List<ProductData> getProductData(@RequestParam(required = false) String pageNum,
            @RequestParam(required = false) String numOfItems)
    {
        int pageNumberInt = StringUtils.isNotBlank(pageNum) && Integer.parseInt(pageNum) > 0 ? Integer.parseInt(pageNum)-1 : DEFAULT_PAGE_NUM;
        int numOFItemsInt = StringUtils.isNotBlank(numOfItems) && Integer.parseInt(numOfItems) > 0 ? Integer.parseInt(numOfItems) : DEFAULT_NUM_OF_ITEMS;

        return productService.getProducts(pageNumberInt, numOFItemsInt);
    }
}

package com.tjx.ecomm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WarehouseController
{

    @RequestMapping("/")
    public String homePage() {
        return "home";
    }

}

package com.tjx.ecomm.converter;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.exceptions.DuplicateProductException;
import com.tjx.ecomm.populators.Populator;
import com.tjx.ecomm.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component public class ProductDataToProductConverter implements Converter<ProductData, Product>
{
   @Autowired private ProductRepo repo;

   @Autowired private List<Populator<ProductData, Product>> populators;

   @Override
   public Product convert(final ProductData source)
   {
      final Product target = getOrCreateProduct(source);
      getPopulators().forEach(populator -> populator.populate(source, target));
      return target;
   }

   private Product getOrCreateProduct(final ProductData source)
   {
      if (source.isPersisted())
      {
         return repo.findProductBySkuCode(source.getSkuCode());
      }
      else
      {
         final Product existingProduct = repo.findProductBySkuCode(source.getSkuCode());
         if (existingProduct != null)
         {
            throw new DuplicateProductException(String.format("Product with skuCode [%s] already exists.", source.getSkuCode()));
         }
         return new Product(source.getSkuCode());
      }
   }

   public List<Populator<ProductData, Product>> getPopulators()
   {
      return populators;
   }

   public void setPopulators(final List<Populator<ProductData, Product>> populators)
   {
      this.populators = populators;
   }
}

package com.tjx.ecomm.converter;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.populators.Populator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component public class ProductToProductDataConverter implements Converter<Product, ProductData>
{
   @Autowired private List<Populator<Product, ProductData>> populators;

   @Override
   public ProductData convert(final Product source)
   {
      final ProductData target = new ProductData();
      getPopulators().forEach(populator -> populator.populate(source, target));
      return target;
   }

   public List<Populator<Product, ProductData>> getPopulators()
   {
      return populators;
   }

   public void setPopulators(List<Populator<Product, ProductData>> populators)
   {
      this.populators = populators;
   }
}

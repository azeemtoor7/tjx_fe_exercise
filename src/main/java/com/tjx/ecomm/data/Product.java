package com.tjx.ecomm.data;

import javax.persistence.*;
import java.util.Map;

@Entity public class Product
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique=true)
    private String skuCode;

    @Column(length = 256)
    private String name;

    @ElementCollection
    private Map<String, Integer> stock;

    protected Product()
    {
    }

    public Product(final String skuCode, final String name)
    {
        this.skuCode = skuCode;
        this.name = name;
    }

    public Product(final String skuCode)
    {
        this.skuCode = skuCode;
    }

    @Override public String toString()
    {
        return String.format("Product[id=%d, skuCode='%s', name='%s']", id, skuCode, name);
    }

    public Long getId()
    {
        return id;
    }

    public String getSkuCode()
    {
        return skuCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Map<String, Integer> getStock()
    {
        return stock;
    }

    public void setStock(Map<String, Integer> stock)
    {
        this.stock = stock;
    }
}

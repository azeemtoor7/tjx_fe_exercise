package com.tjx.ecomm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class ProductData
{
    @NotEmpty
    @Pattern(regexp = "^\\d{10}$")
    private String skuCode;

    @NotEmpty
    @Size(max=256)
    @Pattern(regexp = "^[^\\?]+$")
    private String name;

    @JsonProperty("stock")
    private String totalStockQty;

    @JsonIgnore
    private boolean persisted;

    @Valid
    @JsonIgnore
    private List<StockData> stocks;

    public String getSkuCode()
    {
        return skuCode;
    }

    public void setSkuCode(String skuCode)
    {
        this.skuCode = skuCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTotalStockQty()
    {
        return totalStockQty;
    }

    public void setTotalStockQty(String totalStockQty)
    {
        this.totalStockQty = totalStockQty;
    }

    public List<StockData> getStocks()
    {
        return stocks;
    }

    public void setStocks(List<StockData> stocks)
    {
        this.stocks = stocks;
    }

    public boolean isPersisted()
    {
        return persisted;
    }

    public void setPersisted(boolean persisted)
    {
        this.persisted = persisted;
    }
}

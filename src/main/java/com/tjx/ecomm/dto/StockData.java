package com.tjx.ecomm.dto;

public class StockData
{
   private String qty;

   private String warehouse;

   public String getQty()
   {
      return qty;
   }

   public void setQty(String qty)
   {
      this.qty = qty;
   }

   public String getWarehouse()
   {
      return warehouse;
   }

   public void setWarehouse(String warehouse)
   {
      this.warehouse = warehouse;
   }
}

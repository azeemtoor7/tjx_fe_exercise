package com.tjx.ecomm.exceptions;

public class DuplicateProductException extends RuntimeException
{
   public DuplicateProductException(final String message)
   {
      super(message);
   }
}

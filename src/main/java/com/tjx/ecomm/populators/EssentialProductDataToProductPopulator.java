package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;

public class EssentialProductDataToProductPopulator implements Populator<ProductData, Product>
{
   @Override
   public void populate(final ProductData source, final Product target)
   {
      target.setName(source.getName());
   }
}

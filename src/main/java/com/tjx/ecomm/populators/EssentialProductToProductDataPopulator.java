package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;

public class EssentialProductToProductDataPopulator implements Populator<Product, ProductData>
{
   public static final String SKU_PREFIX = "#";

   @Override
   public void populate(final Product source, final ProductData target)
   {
      target.setSkuCode(SKU_PREFIX + source.getSkuCode());
      target.setName(source.getName());
      target.setPersisted(true);
   }


}

package com.tjx.ecomm.populators;

public interface Populator<SOURCE, TARGET>
{
   void populate(SOURCE source, TARGET target);
}

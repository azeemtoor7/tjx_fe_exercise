package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.dto.StockData;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StockInformationProductDataToProductPopulator implements Populator<ProductData, Product>
{
   @Override
   public void populate(final ProductData source, final Product target)
   {
      target.setStock(convertStockDataToStock(source.getStocks()));
   }

   protected Map<String, Integer> convertStockDataToStock(final List<StockData> stockData)
   {
      return stockData != null
               ? stockData.stream()
                  .filter(e -> StringUtils.isNotEmpty(e.getWarehouse()) )
                  .collect(Collectors.groupingBy(StockData::getWarehouse,
                        Collectors.summingInt(stock -> convertQty(stock.getQty()))))
               : new HashMap<>();
   }

   private int  convertQty(final String qty)
   {
      return !StringUtils.isNumeric(qty) ? 0 : Integer.parseInt(qty);
   }
}

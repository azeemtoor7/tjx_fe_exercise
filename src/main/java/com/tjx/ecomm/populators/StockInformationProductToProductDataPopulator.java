package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.dto.StockData;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StockInformationProductToProductDataPopulator implements Populator<Product, ProductData>
{

   public static final String NO_STOCK = "0";

   @Override
   public void populate(final Product source, final ProductData target)
   {
      target.setTotalStockQty(aggregateStocks(source));
      target.setStocks(convertStocks(source.getStock()));

   }

   protected List<StockData> convertStocks(final Map<String, Integer> stocks)
   {
      if (stocks == null)
      {
         return Collections.emptyList();
      }

      return stocks.entrySet().stream().map(
               entry -> {
                  final StockData stockData = new StockData();
                  stockData.setWarehouse(entry.getKey());
                  stockData.setQty(String.valueOf(entry.getValue()));
                  return stockData;
               }
      ).collect(Collectors.toList());
   }

   protected String aggregateStocks(final Product product)
   {
      if (product.getStock() != null)
      {
         final int totalStock = product.getStock().values().stream().mapToInt(Integer::intValue).sum();
         return String.valueOf(totalStock);
      }
      return NO_STOCK;
   }
}

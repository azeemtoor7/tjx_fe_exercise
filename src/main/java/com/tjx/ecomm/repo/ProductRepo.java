package com.tjx.ecomm.repo;

import com.tjx.ecomm.data.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepo extends PagingAndSortingRepository<Product, Long>
{
   Product findProductBySkuCode(String sku);
}

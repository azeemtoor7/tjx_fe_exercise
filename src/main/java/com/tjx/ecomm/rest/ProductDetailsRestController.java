package com.tjx.ecomm.rest;

import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/rest/v1")
public class ProductDetailsRestController
{
   @Autowired
   private ProductService productService;

   @GetMapping("/product")
   public ProductData getProduct(@RequestParam(value = "skuCode") String skuCode)
   {
      return productService.getProduct(skuCode);
   }
}

package com.tjx.ecomm.services;

import com.tjx.ecomm.dto.ProductData;

import java.util.List;

public interface ProductService
{
    List<ProductData> getAllProducts ();

    List<ProductData> getProducts (int from, int to);

    ProductData getProduct(String sku);

    void createOrUpdateMethod(ProductData product);

}

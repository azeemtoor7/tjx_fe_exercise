package com.tjx.ecomm.services.impl;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.exceptions.ProductNotFoundException;
import com.tjx.ecomm.repo.ProductRepo;
import com.tjx.ecomm.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

@Component public class DefaultProductService implements ProductService
{

    @Autowired private ProductRepo repo;

    @Autowired private Converter<Product, ProductData> productToProductDataConverter;

    @Autowired private Converter<ProductData, Product> productDataToProductConverter;

    @Override public List<ProductData> getAllProducts()
    {
        // Create an empty list
        final List<ProductData> list = new ArrayList<>();

        final Iterable<Product> productIterable = repo.findAll();

        if (null != productIterable)
        {
            // Add each element of iterator to the List
            StreamSupport.stream(productIterable.spliterator(), false)
                     .map(e -> productToProductDataConverter.convert(e))
                     .forEach(list::add);
        }

        return list;
    }

    @Override
    public List<ProductData> getProducts(int from, int to)
    {

        // Create an empty list
        final List<ProductData> list = new ArrayList<>();

        Pageable pageElements = PageRequest.of(from, to);

        final Iterable<Product> productIterable = repo.findAll(pageElements);

        if (null != productIterable)
        {
            // Add each element of iterator to the List
            StreamSupport.stream(productIterable.spliterator(), false)
                    .map(e -> productToProductDataConverter.convert(e))
                    .forEach(list::add);
        }

        return list;
    }

    @Override
    public ProductData getProduct(final String sku)
    {
        final Product productBySkuCode = repo.findProductBySkuCode(sanitize(sku));

        if (productBySkuCode == null)
        {
            throw new ProductNotFoundException(String.format("Product with sku [%s] not found", sku));
        }
        return productToProductDataConverter.convert(productBySkuCode);
    }

    @Override
    public void createOrUpdateMethod(final ProductData productData)
    {
        final Product convertProduct = productDataToProductConverter.convert(productData);
        repo.save(convertProduct);
    }

    protected String sanitize(final String sku)
    {
        return sku != null && sku.length() > 0 && sku.startsWith("#") ? sku.substring(1) : sku;
    }
}

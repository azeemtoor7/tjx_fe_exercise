$(document).ready(function () {

    $("#stocks").on('click', '.btn-remove-row', function () {
        $(this).closest('tr').remove();
    });

    $('.btn-add-row').click(function() {

        var index = $('#stocks tr').length - 1;
        $('#stocks')
            .append(`<tr>` +
            `<td><input type="text" name="stocks[${index}].warehouse"/></td>` +
            `<td><input type="text" name="stocks[${index}].qty" class="txt-add-stock" onkeypress="return isNumberKey(event)" maxlength="4"/></td>` +
            `<td><input type="button" class="btn-remove-row" value="Remove"/></td>` +
            `</tr>`);
        });

});

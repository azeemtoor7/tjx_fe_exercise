<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
<body>

<h1><spring:message code="home.welcome" text="Welcome"/></h1>

<h2><a href="/productList"><spring:message code="home.product.list.link" text="Product List"/></a></h2>
<h2><a href="/productListPaginated"><spring:message code="home.product.list.paginated.link" text="Product List Paginated"/></a></h2>
<h2><a href="/addProduct"><spring:message code="home.add.product.link" text="Add Product"/></a></h2>
</body>
</html>

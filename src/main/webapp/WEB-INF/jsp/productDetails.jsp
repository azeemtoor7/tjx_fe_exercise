<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<link href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet">
<h1><spring:message code="productDetails.title" text="Product Details"/></h1>

<script language="javascript">

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<form:form method="POST" action="/productDetails" modelAttribute="product">

    <div>
        <table id="tabId">
            <tr>
                <th><spring:message code="productDetails.sku" text="Sku"/></th>
                <td>
                    <c:choose>
                        <c:when test="${product.persisted}">
                            ${product.skuCode}
                            <form:hidden path="skuCode"
                                         value="${fn:substring(product.skuCode, 1, 11)}"/>

                        </c:when>
                        <c:otherwise>
                            <form:input path="skuCode" onkeypress="return isNumberKey(event)" maxlength="10"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><form:errors path="skuCode"/></td>
            </tr>
            <tr>
                <th><spring:message code="productDetails.name" text="Name"/></th>
                <td><form:input path="name"/></td>
                <td><form:errors path="name" /></td>
            </tr>
            <tr>
                <th><spring:message code="productDetails.stock" text="Name"/></th>
                <td>${product.totalStockQty}</td>
                <td><form:errors path="totalStockQty"/></td>
            </tr>
        </table>
    </div>

    <div>
        <div>
            <h2><spring:message code="productDetails.stock.details"/></h2>
        </div>
        <div>
            <input type="button" value="Add Stock" class="btn-add-row"/>
        </div>
    </div>

    <div>
        <table id="stocks">
            <tr>
                <th><spring:message code="productDetails.warehouse"/></th>
                <th><spring:message code="productDetails.stock.qty"/></th>
                <th><spring:message code="productDetails.action"/></th>
            </tr>
            <c:forEach items="${product.stocks}" var="stock" varStatus="status">
                <tr>
                    <td><form:input path="stocks[${status.index}].warehouse"
                                    value="${stock.warehouse}"/>
                        <form:errors path="stocks[${status.index}].warehouse"/>
                    </td>
                    <td><form:input path="stocks[${status.index}].qty" value="${stock.qty}" cssClass="txt-add-stock" onkeypress="return isNumberKey(event)" maxlength="4"/></td>
                    <td><input type="button" value="Remove" class="btn-remove-row"></td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <div class="add-product-actions">
        <c:choose>
            <c:when test="${product.persisted}">
                <input type="submit" value="Save Product" class="shadow-button">
            </c:when>
            <c:otherwise>
                <input type="submit" value="Add Product">
            </c:otherwise>
        </c:choose>
        <form:hidden path="persisted"/>
    </div>
</form:form>

<a href="/productList"><spring:message code="productDetails.backlink" text="<<< back to products"/></a>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>



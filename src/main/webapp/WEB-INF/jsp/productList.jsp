<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link href="css/style.css" rel="stylesheet">

<!-- vue app will be mounted on #app  -->
<div id="app">
    <h1><spring:message code="productlist.title" text="Full Product List"/></h1>
    <div class="searchForm">
        <spring:message code="productlist.search" text="Search" var="searchLabel"/>
        <input v-model="filter" placeholder="Search product by name" name="searchBox" type="search"/>
    </div>
    <table>
        <thead>
            <tr>
                <th><spring:message code="productlist.sku" text="Sku"/></th>
                <th><spring:message code="productlist.name" text="Name"/></th>
                <th><spring:message code="Productlist.price" text="Price"/></th>
                <th><spring:message code="productlist.edit.product" text="Edit"/></th>
            </tr>
        </thead>

        <tbody>
            <!-- Loops through the products returned from API -->
            <tr v-for="(prod, index) in filteredProducts" :key="prod.skuCode">
                <td>{{prod.id}}</td>
                <td>{{prod.name}}</td>
                <td>{{prod.price}}</td>
                <td>
                    <a href="#"><spring:message code="productlist.edit.product" text="Edit"/></a>
                </td>
            </tr>
        </tbody>
    </table>

    <a href="/"><spring:message code="productlist.backlink" text="<<< back"/></a>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/axios.min.js"></script>

<script>
    // turns on development mode
    Vue.config.productionTip = false;

    new Vue({
        el: '#app',
        data: {
            filter: '',
            productData: []
        },
        created: function() {
            this.getProducts();
        },
        computed: {
            // returns array of filtered products on the base of search key
            filteredProducts() {
                return this.productData.filter(product => {
                    var pName = product.name.toLowerCase();
                    var searchTerm = this.filter.toLowerCase();

                    return pName.includes(searchTerm);
                });
            }
        },
        methods: {
            // ajax call to product api to get products
            getProducts: function() {
                var url = 'https://gorest.co.in/public-api/products';
                axios.get(url)
                    .then(res => {
                        this.productData = res.data.data;
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        }
    });
</script>
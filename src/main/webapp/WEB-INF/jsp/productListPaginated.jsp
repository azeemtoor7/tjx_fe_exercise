<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link href="css/style.css" rel="stylesheet">

<!-- vue app will be mounted on #app  -->
<div id="app">
    <h1><spring:message code="productlist.paginated.title" text="Full Product List"/></h1>
    <table>
        <thead>
            <tr>
                <th><spring:message code="productlist.sku" text="Sku"/></th>
                <th><spring:message code="productlist.name" text="Name"/></th>
                <th><spring:message code="Productlist.price" text="Price"/></th>
                <th><spring:message code="productlist.edit.product" text="Edit"/></th>
            </tr>
        </thead>

        <tbody>
            <tr v-for="(prod, index) in products" :key="prod.id">
                <td>{{prod.id}}</td>
                <td>{{prod.name}}</td>
                <td>{{prod.price}}</td>
                <td>
                    <a href="#"><spring:message code="productlist.edit.product" text="Edit"/></a>
                </td>
            </tr>
        </tbody>
    </table>

    <a href="/"><spring:message code="productlist.backlink" text="<<< back"/></a>

    <div class="showMore">
        <a href="#" @click.prevent="getProducts()"><spring:message code="productlist.showmore" text="show more"/></a>
    </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/axios.min.js"></script>

<script>
    // development mode
    Vue.config.productionTip = false;

    var vm = new Vue({
        el: '#app',
        data: {
            totalPages: 11,
            pageNo: 1,
            products: []
        },
        created: function() {
            this.getProducts();
        },
        methods: {
            getProducts: function() {
                // product api url
                var url = 'https://gorest.co.in/public-api/products?page=' + this.pageNo;

                // calls to api if a page exists
                if(this.pageNo < this.totalPages) {
                    axios.get(url)
                        .then(res => {
                            // appends the product data with existing products array
                            this.products = this.products.concat(res.data.data);
                            this.pageNo++;

                            // adds current page products and page no to history
                            // it does not support localhost as url
                            history.pushState(
                                {products: this.products},
                                '',
                                ''
                            );
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }
            }
        }
    });
</script>
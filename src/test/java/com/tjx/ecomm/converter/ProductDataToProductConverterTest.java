package com.tjx.ecomm.converter;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.exceptions.DuplicateProductException;
import com.tjx.ecomm.populators.Populator;
import com.tjx.ecomm.repo.ProductRepo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ProductDataToProductConverterTest
{

   @InjectMocks
   private ProductDataToProductConverter converter = new ProductDataToProductConverter();

   private List<Populator<ProductData, Product>> populators = new ArrayList<>();

   @Mock
   private ProductRepo repo;

   @Mock
   private Populator<ProductData, Product> populator1;

   @Before
   public void setup()
   {
      MockitoAnnotations.initMocks(this);

      populators.add(populator1);
      converter.setPopulators(populators);
   }

   @Test
   public void testConvertFor_editProductOperation()
   {
      final Product target = mock(Product.class);
      when(repo.findProductBySkuCode("0001")).thenReturn(target);

      final ProductData source = aProductData();
      converter.convert(source);

      verify(populator1).populate(source, target);
   }

   @Test
   public void testConvertFor_createProductOperation_forNonExistingProduct()
   {
      final ProductData source = aNonExistingProductData();
      when(repo.findProductBySkuCode("0001")).thenReturn(null);

      final Product convertedProduct = converter.convert(source);

      assertThat(convertedProduct.getSkuCode()).isEqualTo("0001");
   }

   @Test(expected = DuplicateProductException.class)
   public void testConvertFor_createProductOperation_forExistingProduct()
   {
      final Product target = mock(Product.class);
      when(repo.findProductBySkuCode("0001")).thenReturn(target);

      final ProductData source = aNonExistingProductData();
      final Product convertedProduct = converter.convert(source);

      assertThat(convertedProduct.getSkuCode()).isEqualTo("0001");
   }

   private ProductData aProductData()
   {
      final ProductData productData = new ProductData();
      productData.setSkuCode("0001");
      productData.setPersisted(true);
      return productData;
   }

   private ProductData aNonExistingProductData()
   {
      final ProductData productData = aProductData();
      productData.setPersisted(false);
      return productData;
   }

}

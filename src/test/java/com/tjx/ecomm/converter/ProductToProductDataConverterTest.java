package com.tjx.ecomm.converter;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.populators.Populator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class ProductToProductDataConverterTest
{

   @InjectMocks
   private ProductToProductDataConverter converter = new ProductToProductDataConverter();

   @Mock
   private Populator<Product, ProductData> populator;

   @Before
   public void setup()
   {
      MockitoAnnotations.initMocks(this);

      converter.setPopulators(Collections.singletonList(populator));
   }

   @Test
   public void test()
   {
      final Product product = aProduct();

      converter.convert(product);

      verify(populator).populate(eq(product), any(ProductData.class));

   }

   private Product aProduct()
   {
      return new Product("0001", "product 1");
   }
}

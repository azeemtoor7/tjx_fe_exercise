package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EssentialProductDataToProductPopulatorTest
{
   private EssentialProductDataToProductPopulator populator = new EssentialProductDataToProductPopulator();

   @Test
   public void testPopulate()
   {

      final ProductData source = aProductData();
      final Product target = new Product("001", "Original Name");
      populator.populate(source, target);

      assertThat(target.getName()).isEqualTo("Modified Name");
   }

   private ProductData aProductData()
   {
      final ProductData productData = new ProductData();
      productData.setSkuCode("#0001");
      productData.setName("Modified Name");
      return productData;
   }

}

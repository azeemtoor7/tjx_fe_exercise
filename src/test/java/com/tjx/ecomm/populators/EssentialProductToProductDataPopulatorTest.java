package com.tjx.ecomm.populators;

import com.google.common.collect.ImmutableMap;
import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static com.tjx.ecomm.populators.EssentialProductToProductDataPopulator.SKU_PREFIX;

public class EssentialProductToProductDataPopulatorTest
{

   private EssentialProductToProductDataPopulator populator = new EssentialProductToProductDataPopulator();


   @Test
   public void testEssentialPropertiesArePopulated()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProduct(), productData);

      assertThat(productData.getSkuCode()).isEqualTo(SKU_PREFIX + "0001");
      assertThat(productData.getName()).isEqualTo("Product 1");
   }


   private Product aProduct()
   {
      final Product product = new Product("0001", "Product 1");
      product.setStock(ImmutableMap.of("w1", 100));
      return product;
   }

}

package com.tjx.ecomm.populators;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.dto.StockData;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class StockInformationProductDataToProductPopulatorTest
{
   private StockInformationProductDataToProductPopulator populator = new StockInformationProductDataToProductPopulator();

   @Test
   public void testSingleStockEntryIsPopulated()
   {
      final ProductData source = aProductDataWith(aStockDataWith("w1", "100"));
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(1);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("100"));
   }

   @Test
   public void testMultipleStockEntriesArePopulated()
   {
      final ProductData source = aProductDataWith(
               aStockDataWith("w1", "100"),
               aStockDataWith("w2", "300")
      );
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(2);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("100"));
      assertThat(target.getStock()).containsEntry("w2", Integer.valueOf("300"));
   }

   @Test
   public void testNoStockEntryIsPopulated()
   {
      final ProductData source = aProductDataWith();
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(0);
   }

   @Test
   public void testStockEntryQtyZeroIsPopulated()
   {
      final ProductData source = aProductDataWith(aStockDataWith("w1", "0"));
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(1);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("0"));
   }

   @Test
   public void testStockEntryQtyNullIsPopulated()
   {
      final ProductData source = aProductDataWith(aStockDataWith("w1", null));
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(1);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("0"));
   }

   @Test
   public void testMultipleStockEntriesWithSameWarehouse()
   {
      final ProductData source = aProductDataWith(
               aStockDataWith("w1", "10"),
               aStockDataWith("w1", "20")
      );
      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(1);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("30"));
   }

   @Test
   public void testPopulateWithNoStock()
   {
      final ProductData source = aProductData();

      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(0);
   }

   @Test
   public void testPopulateForStockLevelWithNullWarehouse()
   {
      final ProductData source = aProductDataWith(
               aStockDataWith(null, "10"),
               aStockDataWith("w1", "20")
      );

      final Product target = aProduct();

      populator.populate(source, target);

      assertThat(target.getStock()).hasSize(1);
      assertThat(target.getStock()).containsEntry("w1", Integer.valueOf("20"));
   }

   private ProductData aProductData()
   {
      return new ProductData();
   }

   private ProductData aProductDataWith(final StockData... stockData)
   {
      final ProductData productData = aProductData();
      productData.setStocks(Arrays.asList(stockData));
      return productData;
   }

   private StockData aStockDataWith(final String warehouse, final String qty)
   {
      final StockData stockData = new StockData();
      stockData.setWarehouse(warehouse);
      stockData.setQty(qty);
      return stockData;
   }

   private Product aProduct()
   {
      return new Product("0001", "product 1");
   }
}

package com.tjx.ecomm.populators;

import com.google.common.collect.ImmutableMap;
import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StockInformationProductToProductDataPopulatorTest
{
   private StockInformationProductToProductDataPopulator populator = new StockInformationProductToProductDataPopulator();

   @Test
   public void testAggregatedStockIsPopulated()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProduct(), productData);

      assertThat(productData.getTotalStockQty()).isEqualTo("100");
   }

   @Test
   public void testAggregatedStockIsPopulated_WithStocksForMultipleWarehouses()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProductWithStocksForMultipleWarehouses(), productData);

      assertThat(productData.getTotalStockQty()).isEqualTo("300");
   }

   @Test
   public void testAggregatedStockIsPopulated_WithNoStock()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProductWithNoStock(), productData);

      assertThat(productData.getTotalStockQty()).isEqualTo("0");
   }



   @Test
   public void testStockDataIsPopulated()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProduct(), productData);

      assertThat(productData.getStocks()).hasSize(1);
      assertThat(productData.getStocks().get(0).getWarehouse()).isEqualTo("w1");
      assertThat(productData.getStocks().get(0).getQty()).isEqualTo("100");
   }

   @Test
   public void testStockDataIsPopulated_WithStocksForMultipleWarehouses()
   {
      final ProductData productData = new ProductData();
      populator.populate(aProductWithStocksForMultipleWarehouses(), productData);

      assertThat(productData.getStocks()).hasSize(2);
      assertThat(productData.getStocks().get(0).getWarehouse()).isEqualTo("w1");
      assertThat(productData.getStocks().get(0).getQty()).isEqualTo("100");

      assertThat(productData.getStocks().get(1).getWarehouse()).isEqualTo("w2");
      assertThat(productData.getStocks().get(1).getQty()).isEqualTo("200");
   }


   //Todo Potentially use builder pattern to create test data

   private Product aProductWithNoStock()
   {
      return new Product("0001", "Product 1");
   }

   private Product aProduct()
   {
      final Product product = aProductWithNoStock();
      product.setStock(ImmutableMap.of("w1", 100));
      return product;
   }

   private Product aProductWithStocksForMultipleWarehouses()
   {
      final Product product = aProduct();
      product.setStock(ImmutableMap.of("w1", 100, "w2", 200));
      return product;
   }
}

package com.tjx.ecomm.services.impl;

import com.tjx.ecomm.data.Product;
import com.tjx.ecomm.dto.ProductData;
import com.tjx.ecomm.exceptions.ProductNotFoundException;
import com.tjx.ecomm.repo.ProductRepo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class DefaultProductServiceTest
{
    private static int NUM_OF_PRODUCTS = 2;

    @InjectMocks
    private DefaultProductService productService = new DefaultProductService();

    @Mock
    private ProductRepo repo;

    @Mock
    private Converter<Product, ProductData> productProductDataConverter;

    @Mock
    private Converter<ProductData, Product> productDataToProductConverter;

    @Mock
    private ProductData mockProductData1;

    @Mock
    private ProductData mockProductData2;

    private List<Product> productList = new ArrayList<>();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);

        //We can use mock objects here as actual attribute conversion is being tested inside populators
        final Product mockProduct1 = mock(Product.class);
        final Product mockProduct2 = mock(Product.class);
        productList.addAll(Arrays.asList(mockProduct1, mockProduct2));

        when(productProductDataConverter.convert(mockProduct1)).thenReturn(mockProductData1);
        when(productProductDataConverter.convert(mockProduct2)).thenReturn(mockProductData2);
    }

    @Test
    public void testReturningAListOfProducts () {
        Mockito.when(repo.findAll()).thenReturn(productList);

        List<ProductData> productDataList = productService.getAllProducts();
        Assert.assertEquals(NUM_OF_PRODUCTS, productDataList.size());
        Assert.assertTrue(productDataList.contains(mockProductData1));
        Assert.assertTrue(productDataList.contains(mockProductData2));
     }

    @Test
    public void testReturningEmptyListOdProducts () {
        Mockito.when(repo.findAll()).thenReturn(null);

        List<ProductData> productDataList = productService.getAllProducts();
        Assert.assertEquals(0, productDataList.size());
    }

    @Test
    public void testGetProductReturnsExistingProduct()
    {
        final Product mockProduct = mock(Product.class);
        when(repo.findProductBySkuCode("0000000001")).thenReturn(mockProduct);
        final ProductData expectedProductData = mock(ProductData.class);
        when(productProductDataConverter.convert(mockProduct)).thenReturn(expectedProductData);

        final ProductData actualProductData = productService.getProduct("#0000000001");

        Assert.assertEquals(expectedProductData, actualProductData);
    }

    @Test
    public void testCreateOrUpdateMethodSavesProductIntoDo()
    {
        final Product mockProduct = mock(Product.class);
        when(productDataToProductConverter.convert(mockProductData1)).thenReturn(mockProduct);

        productService.createOrUpdateMethod(mockProductData1);

        verify(repo).save(mockProduct);
    }

    @Test(expected = ProductNotFoundException.class)
    public void testGetProductThrowsExceptionIfProductNotFound()
    {
        when(repo.findProductBySkuCode("0000000001")).thenReturn(null);
        productService.getProduct("#0000000001");
    }
}
